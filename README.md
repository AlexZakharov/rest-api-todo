Project: 'REST API TODO'
===============================

INSTALLATION
------------
~~~
1) git clone
2) configure env
3) go run src/todo/main.go or ./main
~~~

API calls
------------

Create
------------
~~~
Type POST 
URL http://localhost:8080/api/v1/todos/
Body: 
title 'stting'
compleated 'int'
~~~

Get by id
------------
~~~
Type GET
URL http://localhost:8080/api/v1/todos/id={ID}
~~~

Get all
------------
~~~
Type GET
URL http://localhost:8080/api/v1/todos/
~~~

Update by id
------------
~~~
Type PUT
URL http://localhost:8080/api/v1/todos/id={ID}
Body: 
title 'stting'
compleated 'int'
~~~

Delete by id
------------
~~~
Type DELETE
URL http://localhost:8080/api/v1/todos/id={ID}
~~~