package main

import (
	"net/http"
	"strconv"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"fmt"
)

var db *gorm.DB

func init() {
	dbUser := `gin`
	dbPassword := `123123`
	dbName := `todo`

	conf := fmt.Sprintf("%v:%v@/%v?charset=utf8&parseTime=True&loc=Local", dbUser, dbPassword, dbName)

	var err error
	db, err = gorm.Open("mysql", conf)

	if err != nil {
		panic("failed to connect database")
	}

	db.AutoMigrate(&todoModel{})
}

func main() {

	router := gin.Default()

	v1 := router.Group("/api/v1/todos")
	{
		v1.POST("/", createTodo)
		v1.GET("/", fetchAllTodo)
		v1.GET("/:id", fetchSingleTodo)
		v1.PUT("/:id", updateTodo)
		v1.DELETE("/:id", deleteTodo)
	}
	router.Run()

}

type todoModel struct {
		gorm.Model
		Title     string `json:"title"`
		Completed int    `json:"completed"`
}

type transformedTodo struct {
		ID        uint   `json:"id"`
		Title     string `json:"title"`
		Completed bool   `json:"completed"`
}

// Add
func createTodo(c *gin.Context) {
	completed, _ := strconv.Atoi(c.PostForm("completed"))
	toDo := todoModel{
		Title: c.PostForm("title"), Completed: completed}
	db.Save(&toDo)
	c.JSON(
		http.StatusCreated, gin.H{
			"status": http.StatusCreated, "message": "Todo item created successfully!", "resourceId": toDo.ID})
}

// Get all
func fetchAllTodo(c *gin.Context) {
	var toDos []todoModel
	var transformedToDos []transformedTodo

	db.Find(&toDos)

	if len(toDos) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	//transforms the todos
	for _, item := range toDos {
		completed := false

		if item.Completed == 1 {
			completed = true
		} else {
			completed = false
		}

		transformedToDos = append(
			transformedToDos, transformedTodo{ID: item.ID, Title: item.Title, Completed: completed})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": transformedToDos})
}

// Get one by id
func fetchSingleTodo(c *gin.Context) {
	var todo todoModel
	todoID := c.Param("id")

	db.First(&todo, todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	completed := false

	if todo.Completed == 1 {
		completed = true
	} else {
		completed = false
	}

	toDo := transformedTodo{ID: todo.ID, Title: todo.Title, Completed: completed}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": toDo})
}

// Update by id
func updateTodo(c *gin.Context) {
	var todo todoModel
	todoID := c.Param("id")

	db.First(&todo, todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	db.Model(&todo).Update("title", c.PostForm("title"))
	completed, _ := strconv.Atoi(c.PostForm("completed"))
	db.Model(&todo).Update("completed", completed)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Todo updated successfully!"})
}

// Delete by id
func deleteTodo(c *gin.Context) {
	var todo todoModel
	todoID := c.Param("id")

	db.First(&todo, todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	db.Delete(&todo)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Todo deleted successfully!"})
}
